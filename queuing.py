import math


def basic_parameters():
    applications = float(input("Введите среднее количество заявок: "))
    service_speed = float(input("Введите скорость обслуживания: "))
    channel = int(input("Введите количество обслуживающих каналов: "))
    return applications, service_speed, channel


def queuing_system_with_failures():
    """СМО с отказами"""
    applications, service_speed, channel = basic_parameters()
    x, i, j = 0, 0, 0
    ro = applications/service_speed

    while x < channel:
        x += 1
        j = j + (ro ** x) / (math.factorial(x))
        i = (1 - (ro ** x) * (1/(1 + j)) / math.factorial(x))
        print("\n_________________________________________",
              "\nКоличество каналов обслуживания: ", x,
              "\nВероятность обслуживания: ", round(i, 2),
              "\nАбсолютная пропускная способность", round(
                  applications * i, 2),
              "\nКоэффициент загрузки СМО: ", round(ro * i/x, 2))


def queuing_system_with_unlimited_waiting_time():
    """СМО с неограниченным временем ожидания"""
    applications, service_speed, channel = basic_parameters()
    ro = applications/service_speed
    x, j = 0, 1

    while x < channel:
        x += 1
        j = j + ((ro) ** x) / (math.factorial(x))
        if (ro/x) > 1 or x-ro == 0:
            print("\n_________________________________________", '\n',
                  "Количество каналов: ", x, "Система не работаспособна")
        else:
            p0 = 1/(j + (ro**(x+1) / (math.factorial(x) * (x - ro))))
            y1 = ((ro)**(x+1)) * p0/((math.factorial(x-1))*(x-ro)**2)
            y2 = y1 + ro
            y3 = y1/applications
            y4 = y2 / applications

            print("\n_________________________________________",
                  "\nКоличество каналов: ", x,
                  "\nВероятность простоя всех каналов: ", round(p0, 2),
                  "\nСреднее число заявок в очереди: ", round(y1, 2),
                  "\nСреднее число заявок в системе: ", round(y2, 2),
                  "\nСреднее время ожидания в очереди: ", round(y3, 2),
                  "\nСреднее время нахождения заявки в системе: ", round(y4, 2), '\n')
            k, b = 0, p0
            while k < x:
                k += 1
                pi = ((ro ** k)/math.factorial(k)) * p0
                print("Вероятность простоя ", k, "канал(ов): ", pi)
                b += pi
            print("Вероятность отсутствия очереди: ", b)


def closed_queuing_system():
    """Закрытые системы массового обслуживания"""
    applications, service_speed, channel = basic_parameters()
    n = int(input("Введите количество потенциальных клиентов: "))
    ro = applications/service_speed

    simple_all_channels, simple_from_channels_to_n, i = 0, 0, 0

    while i <= n:
        if i < channel:
            r = math.factorial(n) * (ro ** i) / \
                (math.factorial(n - i) * math.factorial(i))
            simple_all_channels += r
            i += 1
        else:
            k = math.factorial(n) * ro ** i / (math.factorial(n - i)
                                               * (channel ** (i - channel)) * math.factorial(channel))
            simple_from_channels_to_n += k
            i += 1

    downtime_probability = 1 / \
        (simple_all_channels + simple_from_channels_to_n)
    print("\nИнтенсивность потока заявок: ", round(ro, 2), "  |  ",
          "Вероятность простоя всех каналов: ", round(downtime_probability, 2), "\n")

    j, final_probabilities, average_number_of_applications_in_the_queue = 1, 0, 0
    f = channel + 1
    while j <= n:
        if j < channel:
            the_likelihood_of_a_potential_client = (math.factorial(
                n) * (ro ** j) * downtime_probability)/(math.factorial(n - j) * math.factorial(j))
        elif channel <= j <= n:
            the_likelihood_of_a_potential_client = (math.factorial(n) * (ro ** j) * downtime_probability)/(
                math.factorial(n - j) * (channel ** (j - channel)) * math.factorial(channel))
            if f < n and j == f:
                k = (f - channel) * the_likelihood_of_a_potential_client
                average_number_of_applications_in_the_queue += k
                f += 1
        else:
            print("Ошибка расчетов данных")
        print("Финальная вероятность", j, ":", round(
            the_likelihood_of_a_potential_client, 3))
        final_probabilities = final_probabilities + \
            j * the_likelihood_of_a_potential_client
        j += 1
    average_time_spent_by_the_application_in_the_system = final_probabilities / \
        applications * (n - final_probabilities)
    average_time_spent_in_the_queue_for_the_application = average_time_spent_by_the_application_in_the_system - 1 / service_speed
    absolute_bandwidth = (1 - ro) * service_speed
    average_number_of_clients = n - downtime_probability/ro
    average_relative_loss = average_number_of_clients / n

    print("\nСреднее число занятых каналов, среднее число заявок в системе: ", round(final_probabilities,
          2), "\nСреднее число заявок в очереди: ", round(average_number_of_applications_in_the_queue, 2))
    print("Среднее время нахождения заявки в системе: ", round(average_time_spent_by_the_application_in_the_system, 2),
          "\nСреднее время нахождения заявки в очереди: ", round(average_time_spent_in_the_queue_for_the_application, 2))
    print("Абсолютная пропускная способность: ", absolute_bandwidth)
    print("Среднее число клиентов: ", round(average_number_of_clients, 2),
          "\nСредние относительные потери: ", round(average_relative_loss, 2))


smo = int(input(("""Выберите тип системы массового обслуживание:
1 - СМО с отказами
2 - СМО с неограниченным временем ожидания
3 - Закрытая СМО
""")))

if smo == 1:
    queuing_system_with_failures()
elif smo == 2:
    queuing_system_with_unlimited_waiting_time()
elif smo == 3:
    closed_queuing_system()
